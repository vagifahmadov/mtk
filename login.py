from config import *
from defs import *

# variables
width=700
height=400
x=600
y=80
header="MTK MSoftware"
h1='Login page'
geometry=str(width)+"x"+str(height)+"+"+str(x)+"+"+str(y)

# elements
window = Tk()
window.title(header)
window.geometry(geometry)
window.resizable(FALSE,FALSE)
# elements
window.wait_visibility(window)
window.wm_attributes('-alpha', 0.9)

# header text
loginheader=Label(window, text=h1, height=1, width=100, fg='#fdde0e', bg='#0c446e', justify='center', anchor='center', font='Arial 16 bold',)
loginheader.pack()

# username entry
username=Entry(window)
username.pack()


# username label
username_label=Label(window, text='User:', justify='right')
username_label.pack()

# password entry
password=Entry(window, show='*')
password.pack()


# password label
password_label=Label(window, text='Password:', justify='right')
password_label.pack()

# login button
login_btn=Button(text='Login', command=lambda:login_user(username.get(), password.get()))
login_btn.pack()

# grid
# loginheader.grid(row=0, column=0)
# loginheader.pack()
# username_label.grid(row=1, column=0)
# username_label.pack()
# username.grid(row=1, column=1)
# username.pack()
#
# password_label.grid(row=2, column=0)
# password_label.pack()
# password.grid(row=2, column=1)
# password.pack()
#
# login_btn.grid(row=3, column=1)
# login_btn.pack()

# window.attributes('-fullscreen', True)
# text_label = Label(
#     text='Hey qidi qahbe dunya hey! Sen delikanli olsan yuvarlak olmazdin en bastan.',
#     fg='#0099ff',
#     bg='#ffffcc',
#     font='Helvetica 16 bold italic',
#     width=20,
#     height=10,
#     wraplength=100,
#     justify='right',
#     anchor='center'
# )

# def buttn():
#     text_label['text']='Ne lan bu?'
#     text_label['justify']='center'
#     message['text']='Open message box'
#     message['state']='active'
#     close.pack()
#
# def mess_boxs():
#     mess_box.showinfo('Mesaj qutusu', 'Ne o? Apistin kaldin bakiyorum...')
#
# def quit_prj():
#     quit()
#
# mess_box=messagebox
# button=Button(text='Press', command=buttn )
# message=Button(text='Please press button on button', state='disabled', command=mess_boxs)
# close=Button(text='close', command=quit_prj)
#
# message.pack()
# button.pack()
# text_label.pack()

mainloop()
